function Count (input) {
  if (input < 0) {
    throw new RangeError();
  }
  return countOnes(input);
}

// https://algorithms.tutorialhorizon.com/reverse-the-binary-representation-of-a-number/
const rotateBinary = (binNum) => {
  let res = 0;
  while (binNum > 0) {
    res = res << 1 | (binNum & 1);
    binNum = binNum >> 1;
  }
  return res;
};
// based on https://stackoverflow.com/questions/21071836/enumerate-through-binary-number-bits-in-reverse
const countOnes = (n) => {
  let bitNum = 7;
  let res = [0];
  let revNum = rotateBinary(n);
  // to be more efficient (faster) we do a while loop instead of
  // a traditional ==> for (var i = 7; i >= 0; i--) { ...
  // we only need to evaluate one condition do to the fact that JS will
  // return true/false for a while condition of bitNum--
  // The side effect to these approach is that we need to evaluate also
  // the bit for a 0 value and in a while 0 will not trigger because is  false,
  // so we do the firstBit eval outside of the loop
  let firstBit = n & (1 << 0);
  if (firstBit) {
    // the first byte is a 1 so we add the index (0) to the response object
    res.push(0);
  }
  if (n !== 1) {
    while (bitNum--) {
      let bit = revNum & (1 << bitNum);
      if (bit) {
        res.push(7 - bitNum);
      }
    }
  }
  // finally we count the number of 1 that we have in out array;
  res[0] = res.length - 1;
  return res;
};

module.exports = {Count};

console.log(Count(1));
